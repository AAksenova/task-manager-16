package ru.t1.aksenova.tm.controller;

import ru.t1.aksenova.tm.api.service.IProjectTaskService;
import ru.t1.aksenova.tm.api.сontroller.IProjectController;
import ru.t1.aksenova.tm.api.service.IProjectService;
import ru.t1.aksenova.tm.enumerated.Sort;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectController implements IProjectController {

    public final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void createProject() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        projectService.create(name, description);
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));

        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = projectService.findAll(sort);
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + ": " + project.getStatus());
            index++;
        }
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECTS CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();

        final Project project = projectService.findOneById(id);
        showProject(project);
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        final Project project = projectService.findOneByIndex(index);
        showProject(project);
    }

    @Override
    public void showProject(final Project project) {
        if (project == null) return;
        System.out.println("PROJECT ID: " + project.getId());
        System.out.println("PROJECT NAME: " + project.getName());
        System.out.println("PROJECT DESCRIPTION: " + project.getDescription());
        System.out.println("PROJECT STATUS: " + Status.toName(project.getStatus()));
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        projectService.updateByIndex(index, name, description);
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        projectService.updateById(id, name, description);
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[ENTER PROJECT ID:]");
        final String id = TerminalUtil.nextLine();

        final Project project = projectService.findOneById(id);
        projectTaskService.removeTaskToProject(project.getId());
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("[ENTER PROJECT INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;

        final Project project = projectService.findOneByIndex(index);
        projectTaskService.removeTaskToProject(project.getId());
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();

        projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void completeProjectById() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();

        projectService.changeProjectStatusById(id, Status.COMPLETED);
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER PROJECT STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();

        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusByIndex(index, status);
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();

        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusById(id, status);
    }

}
